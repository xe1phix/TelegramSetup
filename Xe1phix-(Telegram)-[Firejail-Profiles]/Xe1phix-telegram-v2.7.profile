# Firejail profile for telegram
# This file is overwritten after every install/update
# Persistent local customizations
include /etc/firejail/telegram.local
# Persistent global definitions
include /etc/firejail/globals.local

noblacklist ${HOME}/Downloads/
noblacklist ${HOME}/.TelegramDesktop
noblacklist ${HOME}/.local/share/TelegramDesktop
noblacklist ${HOME}/.local/share/applications
noblacklist ${HOME}/.local/share/icons
noblacklist ${HOME}/.local/share/TelegramDesktop/tdata

whitelist ${HOME}/Downloads/
whitelist ${HOME}/.TelegramDesktop
whitelist ${HOME}/.local/share/TelegramDesktop
whitelist ${HOME}/.local/share/applications
whitelist ${HOME}/.local/share/icons
whitelist ${HOME}/.local/share/TelegramDesktop/tdata

include /etc/firejail/disable-common.inc
include /etc/firejail/disable-devel.inc
include /etc/firejail/disable-interpreters.inc
include /etc/firejail/disable-programs.inc


dns 139.99.96.146
dns 37.59.40.15
dns 185.121.177.177

caps.drop all
netfilter
nodvd
nonewprivs
noroot
notv
protocol unix,inet,netlink
seccomp

disable-mnt
private-dev
private-cache
private-tmp

noexec ${HOME}
noexec /tmp
